package com.auth.clientc.redentials;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class ClientCredentialsApplication {

    public static void main(String[] args) {
        SpringApplication.run(ClientCredentialsApplication.class, args);
    }

    @GetMapping(value = "/helloWorld")
    public String helloWorld(){
        return "Resource service: Client credentials flow works";
    }

}
